# Project Willy
# Version: 1.3
This project aims to create short two line poems from lines found in tweets.

# Installation
1. Install pip: `sudo apt-get install python3-pip`
2. Install tweepy: `pip install tweepy`
3. Get graphics.py: Search for graphics.py by J.M. Zelle

# Usage
The users uses the GUI to generate poems by clicking on the start button, he/she
can also share the poem by using the share button. 

To send a tweet automatically ever hour, start tweet_every_hour.py in a terminal.

# Contributing
1. Add: `git add file(s)`
2. Commit your changes: `git commit -m 'comment what you comitted'`
3. Push: `git push -u willy master`
4. Submit a pull request 

# Project Members
Willem Datema,
Chi Sam Mac &
Corben Poot