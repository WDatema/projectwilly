#!/usr/bin/env python3
#Based of the weekly exercises and tweaked so it would work for our program. 

from gui_model import OrSwitchesModel
from poem_generator import TweetView
from gui_controller import GUIController

GUIController(OrSwitchesModel(),TweetView()).run()
