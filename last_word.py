#!/usr/bin/env python3

import sys

def last_word(string):
"""A function that prints the last word from a given input"""
    for line in string:
        word_list = line.split()
        return word_list[-1]






