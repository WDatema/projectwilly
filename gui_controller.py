#This part is derived from the weekly exercises and the programs found there.

import time
class GUIController:

    def __init__(self, model, view):
        self.model = model
        self.view = view

    def run(self):
        """Makes sure the switch actions are carried out"""
        self.view.show()
        while True:
            self.view.display(self.model)
            action = self.view.get_action()
            if action == 'generated_tweets':
                self.model.set_generate(not self.model.is_generated())
            elif action == 'share':
                self.model.set_share(not self.model.is_shared())
            elif action == 'quit':
               break
            else:
               raise Exception('Invalid action: {}'.format(action))
        self.view.hide()

