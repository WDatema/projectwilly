#!/usr/bin/python3

def tokenize(string):
	"""Tokenizes the words in a string, and returns the tokenized string"""
	last_word = string.split()[-1]
	last_word_clean = "".join([c if c not in '!,1234567890`~$%^&*()"./\}{[]<>;:-_+=' else ' ' for c in last_word])
	return " ".join(string.split()[:-1]) + ' ' + last_word_clean

if __name__ == '__main__':
	the_input = input('Enter your input:')
	print(tokenize(the_input))