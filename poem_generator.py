#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#This part is also derived from the weekly exercises and was tweaked so that it would suit our needs. 

from graphics import *
from graphics_util import *
import gzip, os, fnmatch, random, sys, re
from collections import defaultdict
from tokenization import tokenize
from indexed_tweets import indexed_dict
from twietweets import find_rhyming_tweets
#from twietweetbot import tweet_something

class Label:
     def __init__(self,win,center,width,height,label):
        """Creates a label in which text can be displayed"""
        w,h = width/2.0, height/2.0
        x,y = center.getX(), center.getY()
        self.xmax, self.xmin = x+w, x-w
        self.ymax, self.ymin = y+h, y-h
        p1 = Point(self.xmin, self.ymin)
        p2 = Point(self.xmax, self.ymax)
        self.rect = Rectangle(p1,p2)
        self.rect.setFill("LightBlue2")
        self.rect.draw(win)
        self.rect.setWidth(2)
        self.label = Text(center, label)
        self.label.draw(win)
        self.label.setFill("white")

class TweetView:
    def show(self):
        """Creates the GUI and the label"""
        self.win = GraphWin("Poems",1280,720)
        self.background = Image(Point(640,360), "twitter.png")
        self.background.draw(self.win)
        banner = Text(Point(300,30),"TwieTweets")
        banner.setSize(24)
        banner.setFill("white")
        banner.setStyle("bold")
        banner.draw(self.win)
        label = Label(self.win,Point(640,250),800,150,"")

    def display(self, model):
        """Creates the GUI"""
        if model.is_generated():
            self.generate = Image(Point(320, 540), "generate.png")
        else:
            self.generate = Image(Point(320, 540), "generate.png")
        self.generate.draw(self.win)
        if model.is_shared():
            self.share = Image(Point(640, 540), "twitter_button.png")
        else:
            self.share = Image(Point(640, 540), "twitter_button.png")
        self.share.draw(self.win)
        if model.is_quit():
            self.quit = Image(Point(960, 540), "exit.png")
        else:
            self.quit = Image(Point(960, 540), "exit.png")
        self.quit.draw(self.win)

    def get_action(self):
        while True:
            try:
                command = self.win.getMouse()
                if is_within(command, self.generate):
                    self.generate.undraw()
                    tweets = find_rhyming_tweets()
                    tweet1 = tweets.split('\n')[0]
                    tweet2 = tweets.split('\n')[1]
                    user1 = tweet1.split()[0]
                    user2 = tweet2.split()[0]
                    rest_tweet1 = tweet1.split()[1:]
                    rest_tweet2 = tweet2.split()[1:]
                    text_user1 = Text(Point(340,235), user1)
                    text_user1.setSize(12)
                    text_user1.setFill("blue")
                    text_user1.setStyle("bold")
                    text_tweet1 = Text(Point(640,235), rest_tweet1)
                    text_tweet1.setSize(12)
                    text_tweet1.setFill("black")
                    text_tweet1.setStyle("bold")
                    text_user2 = Text(Point(340,275), user2)
                    text_user2.setSize(12)
                    text_user2.setFill("blue")
                    text_user2.setStyle("bold")
                    text_tweet2 = Text(Point(640,275), rest_tweet2)
                    text_tweet2.setSize(12)
                    text_tweet2.setFill("black")
                    text_tweet2.setStyle("bold")
                    Label(self.win,Point(640,250),800,150,"")
                    text_user1.draw(self.win)
                    text_tweet1.draw(self.win)
                    text_user2.draw(self.win)
                    text_tweet2.draw(self.win)
                    return 'generated_tweets'
                elif is_within(command, self.share):
                    self.share.undraw()
                    os.system("python3 twietweetbot.py")
                    return 'share'
                elif is_within(command, self.quit):
                    self.win.close()
            except GraphicsError:
                return "quit"        

    def hide(self):
        self.win.close()



