#!/usr/bin/python3

import gzip
from collections import defaultdict
import os
import fnmatch
import random
from tokenization import tokenize
import sys
import re
from collections import defaultdict

def from_tweets():
    path = '/home/willem/projectwilly/projectwilly/'
    configfiles = [os.path.join(dirpath, f) for dirpath, dirnames, files in os.walk(path) for f in fnmatch.filter(files, '*.out.gz')]
    """loads every file in a folder and returns a list per tweet"""
    for file_name in configfiles:
        f=gzip.open(file_name, 'rb')
        file_content = f.read().decode('UTF-8')
        line = file_content.strip()
        tweet = line.strip().split("\n")
        tweets = {"Tweet":tweet}
        return tweets

def linelength():
    tweets = from_tweets()
    tweet1 = random.choice(tweets["Tweet"])
    tweet2 = random.choice(tweets["Tweet"])
    diff=abs(len(tweet1)-len(tweet2))
    if diff <= 6:
        return tokenize(tweet1), tokenize(tweet2)
    else:
        return linelength()

def last_word():
    """A function that prints the last word from a given input"""
    tuple_strings = linelength()
    tweet1 = tuple_strings[0]
    tweet2 = tuple_strings[1]
    word_list1 = tweet1.split()
    word_list2 = tweet2.split()
    return word_list1[-1], word_list2[-1]

def create_pronun_dict():
    """Creates a dictionary with the phonetic information"""
    infile = open('dpw.cd','r')
    word_check_dict = {}
    pronun_list = [line.split("\\") for line in infile]
    for item in pronun_list:
        word = item[1]
        pronunciation = item[4]
        capital_pronun = re.findall('[A-Z][a-z]*', pronunciation)
        no_capital_in_pronun = re.findall('[a-z:][a-z:]*', pronunciation)
        with_colon_pronun = re.findall('[:][a-z]*', pronunciation)
        rhyme = [capital_pronun for capital_pronun in capital_pronun[-1:]] 
        if rhyme == []:
            rhyme = [no_capital_in_pronun for no_capital_in_pronun in no_capital_in_pronun[-1:] if not ':' in no_capital_in_pronun]
            rhyme = [with_colon_pronun[1:] for with_colon_pronun in with_colon_pronun[-1:] if ':' in with_colon_pronun]
        if rhyme == ['']:
            rhyme = []
        if rhyme != []:
            word_check_dict[word] = rhyme
    return word_check_dict
			

def word_pronunciation():
    """Function that checks if two words rhyme, by checking phonetic information of both words"""
    value1 = 1
    value2 = 2
    tweets = from_tweets()
    sentences = tweets['Tweet']
    lijst = []
    rhyme_dict = defaultdict(list)    
    sentence_list = [tokenize(item).strip() for item in sentences]
    last_word_list = [tokenize(item).split()[-1] for item in sentences]
    sentence_rhyme_dict = {rhyme[0]:word}
    for last_word in last_word_list:
        if last_word == word:
            for sentence in sentence_list:
                if last_word == sentence[-len(last_word):] and sentence[-len(last_word)-1] == ' ':
                    lijst.append([rhyme[0], sentence])
    for key,value in lijst:
        rhyme_dict[key].append(value)

    default_dict_rhyme = dict((key, tuple(value)) for key, value in rhyme_dict.items())

    return(default_dict_rhyme) 
                  
           # for key in word_check_dict:
           #     if last_word1 != last_word2 and last_word1 != last_word2[:-1] and last_word1[:-1] != last_word2:
           #         if last_word1 == key:
           #             value1 = word_check_dict[last_word1]
           #         if last_word2 == key:
           #             value2 = word_check_dict[last_word2]
           #         if value1 == value2:
           #             return "RIJM"
           #         else:
           #             word_pronunciation()
           #     word_pronunciation() 
   
def main():
    print(word_pronunciation())
    

if __name__ == "__main__":
	main()            
            
