#This Switch Class is derived from the weekly exercises. We added our own switch and tweaked the model where necessary.

class TweetsModel:

    def __init__(self):
        """
        
        """
        self.generated = False
        self.shared = False
        self.quit = False

    def is_generated(self):
        return self.generated

    def is_shared(self):
        return self.shared
    
    def is_quit(self):
        return self.quit

    def set_generate(self, on):
        self.generate = on

    def set_share(self, on):
        self.share = on

    def set_quit(self, on):
        self.quit = on

    def is_tweet_made(self):
        raise NotImplementedError()


class OrSwitchesModel(TweetsModel):

    def is_tweet_made(self):
        return self.is_generated() or self.is_shared() or self.is_quit()
