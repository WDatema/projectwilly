#!/usr/bin/python3
# -*- coding: utf-8 -*-

import gzip, os, fnmatch, random, sys, re
from collections import defaultdict
from tokenization import tokenize
from indexed_tweets import indexed_dict

def from_tweets():
    path = '/home/willem/projectwilly/projectwilly/'
    configfiles = [os.path.join(dirpath, f) for dirpath, dirnames, files in os.walk(path) for f in fnmatch.filter(files, '*.out.gz')] # found on: http://stackoverflow.com/questions/14798220/how-can-i-search-sub-folders-using-glob-glob-module-in-python
    """loads every file in a folder and returns a list of tweets"""
    for file_name in configfiles:
        f=gzip.open(file_name, 'rb')
        file_content = f.read().decode('UTF-8')
        line = file_content.strip()
        tweet = line.strip().split("\n")
        tweets = {"Tweet":tweet}
        return tweets

def find_rhyming_tweets():
    """Function that checks if two tweets rhyme, by checking phonetic information of both words"""
    tweets = from_tweets()
    check_dict = indexed_dict()
    tweet1 = tokenize(random.choice(tweets["Tweet"])).strip()
    rhyme_list = []
    while rhyme_list == []:
        for key, value in check_dict.items():
            for item in value:
                if tweet1 == item:
                    tweet2 = random.choice(check_dict[key])
                    last_word1 = tweet1.split()[-1]
                    last_word2 = tweet2.split()[-1] 
                    if last_word1 != last_word2 and len(last_word1) > 1 and len(last_word2) > 1:
                        rhyme_list.append((tweet1,tweet2))   
                tweet1 = tokenize(random.choice(tweets["Tweet"])).strip()
    for item in rhyme_list:
        return item[0] + '\n' + item[1]

def main():
    print(find_rhyming_tweets())

if __name__ == "__main__":
	main()            
            
