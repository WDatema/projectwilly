#!/usr/bin/python3
import sys
import re

def main(argv):
    """Function that checks if two words rhyme, by checking phonetic information of both words"""
    infile = open(argv[1], 'r')
    input1 = input('Enter a word: ')
    input2 = input('Enter a second word: ')
    value1 = 1
    value2 = 2
    item_list = [line.split("\\") for line in infile]
    for item in item_list:
        word = item[1]
        pronunciation = item[4]
        capital_pronun = re.findall('[A-Z][a-z]*', pronunciation)
        no_capital_in_pronun = re.findall('[a-z:][a-z:]*', pronunciation)
        with_colon_pronun = re.findall('[:][a-z]*', pronunciation)
        rhyme = [capital_pronun for capital_pronun in capital_pronun[-1:]] 
        if rhyme == []:
            rhyme = [no_capital_in_pronun for no_capital_in_pronun in no_capital_in_pronun[-1:] if not ':' in no_capital_in_pronun]
            rhyme = [with_colon_pronun[1:] for with_colon_pronun in with_colon_pronun[-1:] if ':' in with_colon_pronun]
        if rhyme == ['']:
            rhyme = []
        if rhyme != []:
            word_check_dict = {word:rhyme}
        #    print(word_check_dict)

            for key in word_check_dict:
                if input1 != input2 and input1 != input2[:-1] and input1[:-1] != input2:
                    if input1 == key:
                        value1 = word_check_dict[input1]
                    if input2 == key:
                        value2 = word_check_dict[input2]
                    if value1 == value2:
                        print("RIJM")
                        return 0
                else:
                    return 0

if __name__ == '__main__':
    main(sys.argv)
