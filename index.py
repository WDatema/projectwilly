#!/usr/bin/python3

import gzip
from collections import defaultdict
import os
import fnmatch
from tokenization import tokenize
import re

def from_tweets():
    path = '/home/csm/projectwilly'
    configfiles = [os.path.join(dirpath, f) for dirpath, dirnames, files in os.walk(path) for f in fnmatch.filter(files, '*.out.gz')]
    """loads every file in a folder and returns a list per tweet"""
    tweets_dict = {}
    for file_name in configfiles:
        f=gzip.open(file_name, 'rb')
        file_content = f.read().decode('UTF-8')
        tweets = file_content.strip().split("\n")
        tweets_dict = {"Tweet":tweets}
        return tweets_dict

def create_index():
    """Function that checks if two words rhyme, by checking phonetic information of both words"""
    tweets = from_tweets()
    sentences = tweets['Tweet']
    lijst = []
    rhyme_dict = defaultdict(list)    
    sentence_list = [tokenize(item).strip() for item in sentences if len(item) <= 70]
    last_word_list = [tokenize(item).split()[-1] for item in sentences]
    infile = open('dpw.cd','r')
    item_list = [line.split("\\") for line in infile]
    word_check_dict = {}
    for item in item_list:
        word = item[1]
        rhyme = item[3]
        if rhyme != []:
            if item[3][:1] == "'":
        	    rhyme = item[3][2:]
            else:
                rhyme = item[3][1:]
            word_check_dict[word] = rhyme
            for last_word in last_word_list:
                if last_word == word:
                    for sentence in sentence_list:
                        if last_word == sentence[-len(last_word):] and sentence[-len(last_word)-1] == ' ':
                            lijst.append([rhyme, sentence])
    for key,value in lijst:
            rhyme_dict[key].append(value)

    default_dict_rhyme = dict((key, tuple(value)) for key, value in rhyme_dict.items() if key != '' and len(key) > 1)

    return default_dict_rhyme  

def main():
    print(create_index())

if __name__ == "__main__":
	main()        